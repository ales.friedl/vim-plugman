Default root directory for plugins is:
~~~
~/.vim/plugins/repos/
~~~

Copy `plugman` to the root directory.
~~~
repos
└── ash
    └── ash
        └── plugman
~~~

Copy your other plugins to the root directory too.
The following directory structure is recommended,
but not required.
~~~
repos
├── github.com
│   └── Valloric
│       └── YouCompleteMe
├── drchip.org
│   └── NERD_tree
└── sourceforge.net
    └── increment
...
~~~

Default plugins configuration file is `plugrc.vim`:
~~~
~/.vim/plugins/repos/ash/ash/plugman/plugrc.vim
~~~

Activate your plugins (except plugman) in `plugrc.vim`:
~~~
call plugman#plugin("github.com/Valloric/YouCompleteMe")
~~~

Configure your plugins in `plugrc.vim` file too, you can use config guard
which causes that the configuration (mappings etc.) is only sourced for active plugins:
~~~
if plugman#plugin_config("github.com/Valloric/YouCompleteMe")

let g:ycm_autoclose_preview_window_after_insertion = 1
nnoremap <leader>gd :YcmCompleter GoTo<cr>

endif
~~~

Put into your .vimrc the following lines:
~~~
set runtimepath^=~/.vim/plugins/repos/ash/ash/plugman
source ~/.vim/plugins/repos/ash/ash/plugman/plugin/plugman.vim
~~~

Since now, your plugins are managed with `plugman`, a runtimepath manager.

If you want to modify some files in any plugin without touching original sources,
you can use plugins overlay system. Plugin files under `overs` are sourced first
which works fine with most plugins which use file guards to not source files twice.
~~~
~/.vim/plugins/overs/
~~~

If you want to replace some plugin completely (all files) but without touching
original sources, you can use plugins replacement system. Plugins present in `repls`
are only sourced there, not in `repos`.
~~~
~/.vim/plugins/repls/
~~~

The directory structure is up to you as far as you keep the root in `~/.vim/plugins/repos/`.
The root directory will be configurable in later versions.
~~~
.vim
 └─ plugins
    ├── repos
    │   ├── ash
    │   │   └── ash
    │   │       ├── plugman
    │   │       └── plugrc.vim
    │   ├── drchip.org
    │   │   └── NERD_tree
    │   ├── github.com
    │   │   └── Valloric
    │   │       └── YouCompleteMe
    │   └── vim.org
    │       └── calendar
    │
    ├── overs <- optional directory of enhancements
    │   └── vim.org
    │       └── calendar
    │           └── plugin
    │               └── calendar.vim <- sourced before calendar.vim in repos
    │
    └── repls <- optional directory of replacements
        └── drchip.org
            └── NERD_tree <- sourced instead of plugin in repos
~~~

Comment out or delete the line in `plugrc.vim` to disable the plugin.
~~~
"call plugman#plugin("github.com/Valloric/YouCompleteMe")
~~~
