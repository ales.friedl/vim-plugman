" Example of plugin activation:
"
" call plugman#plugin("github.com/Valloric/YouCompleteMe")
" call plugman#plugin("souceforge.net/increment")
" 
" Example of plugin configuration with guard:
"
" if plugman#plugin_config("github.com/Valloric/YouCompleteMe")
" let g:ycm_autoclose_preview_window_after_insertion = 1
" nnoremap <leader>gd :YcmCompleter GoTo<cr>
" endif
" 
" if plugman#plugin_config("sourceforge.net/increment")
" vnoremap <c-a> :Inc<CR>
" endif
