if exists("g:loaded_plugman") || &cp
  finish
endif
let g:loaded_plugman = 1

function! plugman#plugin(name)
	let l:loaded = 0
	let l:over = '~/.vim/plugins/overs/'.a:name
	if !empty(glob(l:over))
		let l:loaded = 1
		let &runtimepath .= ','.l:over
		"echo "plugin ".l:over
		"execute "set runtimepath+=".l:over
		if !empty(glob(l:over.'/ftdetect'))
			"echo l:over.'/ftdetect/*.vim'
			execute "source ".l:over."/ftdetect/*.vim"
		endif
	endif
	let l:repl = '~/.vim/plugins/repls/'.a:name
	if !empty(glob(l:repl))
		let l:loaded = 1
		let &runtimepath .= ','.l:repl
		"echo "plugin ".l:repl
		"execute "set runtimepath+=".l:repl
		if !empty(glob(l:repl.'/after'))
			let &runtimepath .= ','.l:repl.'/after'
		endif
		if !empty(glob(l:repl.'/ftdetect'))
			"echo l:repl.'/ftdetect/*.vim'
			execute "source ".l:repl."/ftdetect/*.vim"
		endif
	else
		let l:repo = '~/.vim/plugins/repos/'.a:name
		if !empty(glob(l:repo))
			let l:loaded = 1
			let &runtimepath .= ','.l:repo
			"echo "plugin ".l:repo
			"execute "set runtimepath+=".l:repo
			if !empty(glob(l:repo.'/after'))
				let &runtimepath .= ','.l:repo.'/after'
			endif
			if !empty(glob(l:repo.'/ftdetect'))
				"echo l:repo.'/ftdetect/*.vim'
				execute "source ".l:repo."/ftdetect/*.vim"
			endif
		endif
	endif
	if !empty(glob(l:over))
		if !empty(glob(l:over.'/after'))
			let &runtimepath .= ','.l:over.'/after'
		endif
	endif
	if l:loaded == 0
		echom "warning: plugin '".a:name."' not found"
	endif
endfunction

function! plugman#plugin_config(name)
	let l:rtpa = split(&rtp, ',')
	let l:repl = '~/.vim/plugins/repls/'.a:name
	if (index(l:rtpa, l:repl) >= 0)
		return 1
	else
		let l:repo = '~/.vim/plugins/repos/'.a:name
		if (index(l:rtpa, l:repo) >= 0)
			return 1
		endif
	endif
	let l:over = '~/.vim/plugins/overs/'.a:name
	if (index(l:rtpa, l:over) >= 0)
		return 1
	endif
	return 0
endfunction

execute 'source ' . expand('<sfile>:p:h') . '/../plugrc.vim'

" vim: set foldmethod=marker foldmarker=\ {{{,\ }}} foldclose= foldcolumn=0 : */
